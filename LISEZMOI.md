# lancement_ansible

Outils générique de lancement de playbook Ansible. Il est utilisé comme sous-modules git (documentation [Francaise ici](https://git-scm.com/book/fr/v2/Utilitaires-Git-Sous-modules) et [Anglaise ici](https://git-scm.com/docs/git-submodule)).

```shell
$ make
----  Plateforme  ------------------------------------------------------------------------
create-plx   Création squelette inventaire/variables pour PLX
view-vars    Affiche les variables dans vars/main.yml
vault-file   Chiffre/déchiffre le fichier FILE (par défaut le vault.yml de PLX)
----  Cycle de déploiement  --------------------------------------------------------------
everything   Intall and deploy the application
rollback     Rollback the data
playbook     Deploy a specific playbook
install      Installation les applications (Partie root)
deploy       (Re)déploie les applications
clone-requirements Clone les dépots des roles nécessaires définits dans le fichier requirements.yml

------------------------------------------------------------------------------------------
```

## Utilisation de ce sous-module dans les projets

### 1. A la création du projet  

Quand vous êtes au début dans la création du projet et que vous êtes à la racine de votre projet, lancer la commande suivante:

```shell
git submodule add https://gitlab.com/t8384/lancement_ansible
```

Vous obtiendrez un répertoire contenant le sous module ***lancement_ansible*** dans la dernière version du master et un fichier ***.gitmodules*** décrivant les sous-modules du projet, vous pouvez en ajouter d'autres.

```git
[submodule "lancement_ansible"]
path = lancement_ansible
url = https://gitlab.com/t8384/lancement_ansible
```

### 2. Si les sous modules sont vides après un **git clone --recurse-submodules**

Lancer la commande suivante:  

```shell
git submodule update --init
```

Cela permet de télécharger de nouveau les sous modules dans la version attendu (commité dans le projet)

### 3. Mise à jour des sous modules  

#### 3.1. Tous les sous modules d'un coups  

Si vous voulez mettre à jour tous vos sous modules dans la dernière version du master, lancer la commande suivante:  

```shell
git submodule foreach git pull origin master
```

#### 3.2 Un seul sous module  

Lancer les commandes suivantes:

```shell
cd lancement_ansible
git pull origin master
cd ..
git add lancement_ansible
git commit -m "moved lancement_ansible to latest master"
git push
```

### 4. Choisir un tag, une branche  

Pour mettre le sous module dans une version/branche spécifique, exécuter les commandes suivantes:

```shell
cd lancement_ansible
git checkout v1.0
cd ..
git add lancement_ansible
git commit -m "moved lancement_ansible to v1.0"
git push
```

## Rappel pour Ansible, comment remplir le fichier vars/main.yml

```text
# en début de ligne =>  recopie la ligne et tant que commentaire
# = en début de ligne => recopie la ligne
# ? en fin de ligne => valeur non définie  
# ! en fin de ligne => valeur vaulté  
# ~ en fin de ligne => valeur null  
# # => commentaire non reprit dans le fichier de destination.  
```
