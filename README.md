# lancement_ansible

Here is the French [README.md => LIZEZMOI.md](/LIZEZMOI.md)  
Vous trouverez la documentation Française dans le fichier [LIZEZMOI.md](/LIZEZMOI.md)  

Generic Ansible tools to launch playbook. It is used as git submodules (documentation [French here](https://git-scm.com/book/fr/v2/Utilitaires-Git-Sous-modules) et [English here](https://git-scm.com/docs/git-submodule)).

```shell
$ make
----  Platform  --------------------------------------------------------------------------
create-plx   Inventory/variable skeleton creation for PLX
view-vars    Displays variables in vars/main.yml
vault-file   Encrypts/decrypts the FILE file (by default vault.yml for PLX)
----  Deployment cycle  ------------------------------------------------------------------
everything   Intall and deploy the application
rollback     Rollback the data
playbook     Deploy a specific playbook
install      Install the applications (root part)
deploy       deploys(again) applications
clone-requirements Clones the necessary role repositories defined in the requirements.yml file

------------------------------------------------------------------------------------------
```

## Using this sub-module in projects

### 1. At the creation of the project  

When you are at the beginning of creating the project and you are at the root of your project, run the following command:

```shell
git submodule add https://gitlab.com/t8384/lancement_ansible
```

You will get a directory containing the ***lancement_ansible*** submodule in the latest version of the master and a ***.gitmodules*** file describing the project's submodules, you can add others.

```git
[submodule "lancement_ansible"]
path = lancement_ansible
url = https://gitlab.com/t8384/lancement_ansible
```

### 2.If submodules are empty after a **git clone --recurse-submodules**

run the following command:  

```shell
git submodule update --init
```

This allows you to download again the sub-modules in the expected version (commited in the project)

### 3. Update of submodules

#### 3.1. All of submodules in one shot

If you want to update all your submodules to the latest master version, run the following command:  

```shell
git submodule foreach git pull origin master
```

#### 3.2 Only one submodule  

run the following command:

```shell
cd lancement_ansible
git pull origin master
cd ..
git add lancement_ansible
git commit -m "moved lancement_ansible to latest master"
git push
```

### 4. Choose a tag, a branche  

To put the submodule in a specific version/branch, run the following commands:

```shell
cd lancement_ansible
git checkout v1.0
cd ..
git add lancement_ansible
git commit -m "moved lancement_ansible to v1.0"
git push
```

## Reminder for Ansible, how to fill in the vars/main.yml file

```text
# at the beginning of the line => Copy the line as a commented line
# = at the beginning of the line => Copy the line
# ? at end of line => Undefined value
# ! at the end of the line => Vaulted value
# ~ at end of line => Null value
# # => Comment not included in destination file.  
```
