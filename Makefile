SHELL:=/bin/bash
export

###############################################################
# Project variables
###############################################################
# Use .env file to locally override Makefile variables without
# modifying/commiting standard values
# e.g. for temporarily modifying verbosity or using remote docker daemon
# Override only via CLI or in .env.local
PLX?=dev

include lancement_ansible/Makefile.env  # contains default values for the environment variables needed by the Makefile
-include Makefile.env                   # project overrides
-include Makefile.env.local             # uncommitted file with local overrides
-include Makefile.env.${PLX}            # committed environment-specific defaults
-include Makefile.env.${PLX}.local      # uncommitted environment-specific local overrides

###  START OF THE VARIABLE SECTION  #################################
# Internal use only
# The following blocks are not to be edited by the user
###############################################################

###############################################################
# !!! For constructing target controls !!!
###############################################################
# Variables dependent on previous sections
# must not be edited or overridden by .env

PLAYBOOK_DIR?=${PWD}/playbook

LOCAL_USER=$(shell id -u):$(shell id -g)
DOCKER_AS_LOCAL_USER=\
  --mount type=bind,src=/etc/passwd,dst=/etc/passwd,readonly \
  --mount type=bind,src=/etc/group,dst=/etc/group,readonly \
  --mount type=bind,src=/etc/shadow,dst=/etc/shadow,readonly \
  --mount type=bind,src=${HOME},dst=${HOME} \
  --user ${LOCAL_USER}

# Configure Docker options to launch the ansible image
DOCKER_OPTS=\
  ${DOCKER_AS_LOCAL_USER} \
  --mount type=bind,src=${PLAYBOOK_DIR},dst=/srv/ansible \
  --workdir /srv/ansible \
  --env ANSIBLE_FORCE_COLOR=true \
  --env ANSIBLE_VERBOSITY=${ANSIBLE_VERBOSITY_LEVEL}

ifdef DOCKER_EXTRA_VOLUME
ifeq ($(wildcard ${DOCKER_EXTRA_VOLUME}),${DOCKER_EXTRA_VOLUME})
  DOCKER_OPTS+=--mount type=bind,src=${DOCKER_EXTRA_VOLUME},dst=/var/tmp/docker_extra
endif
endif

ifdef DOCKER_CERTS_VOLUME
ifeq ($(wildcard ${DOCKER_CERTS_VOLUME}),${DOCKER_CERTS_VOLUME})
  DOCKER_OPTS+=--mount type=bind,src=${DOCKER_CERTS_VOLUME},dst=/var/tmp/docker_certs
endif
endif

ifdef DEV_MODE
  NO_GALAXY_REQUIREMENT=1
  DOCKER_OPTS+=--volume $(DEFAULT_ROLES_PATH)/:${HOME}/.ansible/roles/
endif
ifdef NO_GALAXY_REQUIREMENT
  DOCKER_OPTS+=--env NO_GALAXY_REQUIREMENT=1
endif

# Expected volumes (source code and sql script)
ifeq ($(shell [ -d ${PWD}/src ] && echo "1"),1)
  DOCKER_OPTS+=--mount type=bind,src=${PWD}/src,dst=/source
endif
ifeq ($(shell [ -d ${PWD}/sql ] && echo "1"),1)
  DOCKER_OPTS+=--mount type=bind,src=${PWD}/sql,dst=/sql
endif

# If the file containing the vault password is defined and present, we mount it in the container
ifdef ANSIBLE_VAULT_PWD_FILE
ifeq ($(wildcard ${ANSIBLE_VAULT_PWD_FILE}),${ANSIBLE_VAULT_PWD_FILE})
  DOCKER_OPTS+=--volume ${ANSIBLE_VAULT_PWD_FILE}:/var/tmp/ansible/vault_pwd
else ifneq (${CI},true)
  DOCKER_OPTS+=-ti
endif
endif

# Configure Ansible options
ANSIBLE_OPTS=--user=${ANSIBLE_USER}

ifdef ANSIBLE_VAULT_PWD_FILE
  ANSIBLE_OPTS+=--vault-password-file /var/tmp/ansible/vault_pwd
endif

ifdef TAG
  ANSIBLE_OPTS+=--tags '$(TAG)'
endif
ifdef LIMIT
  ANSIBLE_OPTS+=--limit '$(LIMIT)'
endif

# Additional options passed by the user
DOCKER_OPTS+=${DOCKER_CUSTOM_OPTS}
ANSIBLE_OPTS+=${ANSIBLE_CUSTOM_OPTS}

# Add possibility in the container to override the vault password file
# * manually (requires docker -ti)
# * via variable ANSIBLE_VAULT_PWD
manage_ansible_vault:=\
  if [[ \"$$CI\" != \"true\" ]]; then \
    if [[ -n '${ANSIBLE_VAULT_PWD_FILE}' && ! -f '/var/tmp/ansible/vault_pwd' && -z '${ANSIBLE_VAULT_PWD}' ]];then \
      echo -n 'Vault password: '; \
      read -s ANSIBLE_VAULT_PWD; \
    fi; \
    if [[ -n '${ANSIBLE_VAULT_PWD}' ]];then \
      ANSIBLE_VAULT_PWD=${ANSIBLE_VAULT_PWD}; \
    fi; \
    if [[ -n \"\$$ANSIBLE_VAULT_PWD\" ]]; then mkdir -p /var/tmp/ansible; echo \"\$$ANSIBLE_VAULT_PWD\" >/var/tmp/ansible/vault_pwd; fi; \
  fi

--: ## ----  Project    ------------------------------------------------------------------------
ansible-init: ## Create common Sructure for Ansible playbook
	mkdir -p playbook/group_vars playbook/inventories playbook/roles playbook/vars
	echo "---" > playbook/group_vars/all.yml
	echo "[all]" > playbook/inventories/hosts.dist
	echo "---" > playbook/vars/main.yml
	echo "---" > playbook/backup.yml
	echo "---" > playbook/install_root.yml
	echo "---" > playbook/requirements.yml
	echo "---" > playbook/rollback.yml
	echo "---" > playbook/start.yml
	echo "---" > playbook/stop.yml
--: ## ----  Plateform  ------------------------------------------------------------------------
create-plx: ## Create the inventory/variables skeleton for PLX
create-plx:
	$(eval ANSIBLE_VAULT_PWD=$(shell cat /dev/urandom | tr -dc "A-Za-z0-9+/_-" | head -c 130))
	if [ -d "${PLAYBOOK_DIR}/inventories/${PLX}" ]; then echo "inventory '${PLX}' already exists"; exit 1; fi
	mkdir -p "${PLAYBOOK_DIR}/inventories/${PLX}/group_vars/all"
	cp "${PLAYBOOK_DIR}/inventories/hosts.dist" "${PLAYBOOK_DIR}/inventories/${PLX}/hosts"
	# TODO: remove the for loop below (specific to certificates for docker)
	# HOSTS=($$(grep -oP "^[a-zA-Z0-9_-]+(?=.*ansible_host=__undefined__)" "${PLAYBOOK_DIR}/inventories/${PLX}/hosts")); \
	# for HOST in $${HOSTS[@]}; do \
	#   IP=($$(2>/dev/null find ${PKI_ROOT_VOLUME}/forge  -wholename "*/$$HOST/*/openssl.cnf" -exec grep -ohP "^IP.1\s*=\s*\K\d+\.\d+\.\d+\.\d+" {} + | sort -u )); \
	#   if [[ $${#IP[@]} -eq 1 ]]; then \
	#     sed -i "/^$$HOST/ s/__undefined__/$${IP[0]}/" "${PLAYBOOK_DIR}/inventories/${PLX}/hosts"; \
	#   fi; \
	# done;
	# Below in order:
	# - filter rows that start with ## (comment not included in destination file)
	# - convert 'dir: "{{ env_dir }}" #="/data/directory"' into 'env_dir }}" #="/data/directory"'
	# - keep only the variable name and the end of line comment
	# - replace '# =' by the line (default value)
	# - replace '# ?' by __undefined__ (undefined)
	# - replace '# !' by {{ vault_XXX }} for the env_XXX variable (vaulted value)
	# - replace '# ~' with ~ (null value in yaml)
	# File comments are kept (no filtering)
	grep -v '^# #' "${PLAYBOOK_DIR}/vars/main.yml" \
	  | grep -oP '(^---|^#.*|:\s*"{{\s*\Kenv_[A-Za-z0-9_]+.*}}.*)' \
	  | sed -E "s/^(env_[A-Za-z0-9_]+)[^#]*(#.*)?/\1: \2/ \
	  ; s/# =// \
	  ; s/# \?\s*$$/__undefined__/ \
	  ; s/^env_([A-Za-z0-9_]+).*# !\s*/env_\1: \"{{ vault_\1 }}\"/ \
	  ; s/# ~\s*$$/~/" \
	  > "${PLAYBOOK_DIR}/inventories/${PLX}/group_vars/all/vars.yml"
	## Generating the vault file
	grep -oP '^---|{{\s*\Kvault_[A-Za-z0-9_]+' "${PLAYBOOK_DIR}/inventories/${PLX}/group_vars/all/vars.yml" \
	  | sed -E '/^vault_/ s/$$/: __undefined__/' \
	  > "${PLAYBOOK_DIR}/inventories/${PLX}/group_vars/all/vault.yml"
	docker run --rm \
	  ${DOCKER_AS_LOCAL_USER} \
	  --mount "type=bind,src=${PLAYBOOK_DIR},dst=/srv/ansible" \
	  --workdir /srv/ansible \
	  --env NO_GALAXY_REQUIREMENT=1 --env ANSIBLE_VERBOSITY=0 \
	  ${ANSIBLE_TOOL_TAGGED_IMAGE} sh -c " \
	    echo ${ANSIBLE_VAULT_PWD} >/tmp/vault_pwd; \
	    ansible-vault encrypt \
	      --vault-password-file /tmp/vault_pwd \"inventories/${PLX}/group_vars/all/vault.yml\" "
	echo "Vault Password : ${ANSIBLE_VAULT_PWD}"
	mkdir -p $$(dirname ${ANSIBLE_VAULT_PWD_FILE})
	echo  ${ANSIBLE_VAULT_PWD} > ${ANSIBLE_VAULT_PWD_FILE}
	chmod 0640 ${ANSIBLE_VAULT_PWD_FILE}
	$(MAKE) view-vars

view-vars: ## Show value of variables from vars/main.yml for PLX
view-vars:
	docker run --tty --rm \
	  ${DOCKER_OPTS} \
	  --env NO_GALAXY_REQUIREMENT=1 \
	  --env ANSIBLE_VERBOSITY=0 \
	  ${ANSIBLE_TOOL_TAGGED_IMAGE} sh -c " \
	    $(manage_ansible_vault); \
	    grep -v "^---" vars/main.yml | sed '/^---/ d; s/^/  /;1 i dump_vars:' >/tmp/dump.yml; \
	    ansible localhost \
	      ${ANSIBLE_OPTS} \
	      -i \"inventories/${PLX}/hosts\" \
	      -e @vars/main.yml -e @/tmp/dump.yml \
	      -m ansible.builtin.copy -a 'dest=/tmp/vars content=\"{{ dump_vars | to_nice_yaml(indent=2, width=500) }}\"' \
	      >/tmp/vars; \
	    grep --color -E '__undefined__|$$' /tmp/vars;"

vault-file: ## Encrypt/decrypt FILE file (by default PLX's vault.yml)
vault-file:
	$(eval FILE?=${PLAYBOOK_DIR}/inventories/${PLX}/group_vars/all/vault.yml)
	$(if $(shell head -1 "${FILE}" | grep "^\$$ANSIBLE_VAULT;"), $(eval ACTION=decrypt), $(eval ACTION=encrypt))
	echo ${DOCKER_OPTS}
	docker run --rm  \
	  ${DOCKER_OPTS} \
	  -v "$$(realpath $$(dirname "${FILE}")):/var/tmp/data" \
	  --env NO_GALAXY_REQUIREMENT=1 \
	  --env ANSIBLE_VERBOSITY=0 \
	  ${ANSIBLE_TOOL_TAGGED_IMAGE} sh -c " \
	    $(manage_ansible_vault); \
	    ansible-vault ${ACTION} \
	      --vault-password-file /var/tmp/ansible/vault_pwd \"/var/tmp/data/$$(basename ${FILE})\";"

--: ##
--: ## ----  Deployment cycle  --------------------------------------------------------------

everything: ## Install (root) and deploy (user) the application
everything:
	$(MAKE) install
	$(MAKE) deploy

_preinstall: # Can be overridden in the project's Makefile

	  #${ANSIBLE_TOOL_TAGGED_IMAGE} sh -c "env && ls -l /usr/bin"

install: ## Install(again) the application (root part)
install: _preinstall
	docker run --rm  \
	  ${DOCKER_OPTS} \
	  ${DOCKER_OPTS_INSTALL} \
	  ${ANSIBLE_TOOL_TAGGED_IMAGE} sh -c " \
	  $(manage_ansible_vault); \
	    ansible-playbook \
	    ${ANSIBLE_OPTS} \
	    ${ANSIBLE_OPTS_INSTALL} \
	    -e @vars/main.yml \
	    -i \"inventories/${PLX}/hosts\" \
	    install_root.yml; "

_predeploy: # Can be overridden in the project's Makefile

deploy: ## Deploy(again) the application (user part)
deploy: _predeploy
	docker run --rm \
	  ${DOCKER_OPTS} \
	  ${DOCKER_OPTS_DEPLOY} \
	  ${ANSIBLE_TOOL_TAGGED_IMAGE} ansible-playbook \
	    ${ANSIBLE_OPTS} \
	    ${ANSIBLE_OPTS_DEPLOY} \
	    -e @vars/main.yml \
	    -i "inventories/${PLX}/hosts" \
	    deploy.yml; 

backup: ## Make a backup via the backup.yml playbook
backup:
	docker run --rm \
	  ${DOCKER_OPTS} \
	  ${ANSIBLE_TOOL_TAGGED_IMAGE} ansible-playbook \
	    ${ANSIBLE_OPTS} \
	    -e @vars/main.yml \
	    -i inventories/${PLX}/hosts \
	    backup.yml; 

rollback: ## Restore via rollback.yml playbook
rollback:
	docker run --rm \
	  ${DOCKER_OPTS} \
	  ${ANSIBLE_TOOL_TAGGED_IMAGE} ansible-playbook \
	    ${ANSIBLE_OPTS} \
			${ANSIBLE_OPTS_ROLLBACK} \
	    -e @vars/main.yml \
	    -i inventories/${PLX}/hosts \
	    rollback.yml; 

_preplaybook: # Can be overridden in the project's Makefile

playbook: ## Launch a specific playbook ANSIBLE_PLAYBOOK
playbook: _preplaybook
	docker run --rm \
	  ${DOCKER_OPTS} \
	  ${DOCKER_OPTS_PLAYBOOK} \
	  ${ANSIBLE_TOOL_TAGGED_IMAGE} ansible-playbook \
	    ${ANSIBLE_OPTS} \
	    ${ANSIBLE_OPTS_PLAYBOOK} \
	    -e @vars/main.yml \
	    -i inventories/${PLX}/hosts \
	    $(ANSIBLE_PLAYBOOK); 

clone-requirements: ## Clone the repositories of the necessary roles defined in the requirements.yml file
	lancement_ansible/clone-requirements.sh

ifndef DEBUG
.SILENT:
endif

.PHONY: *

COLOR_SECTION = \033[33m
COLOR_TARGET  = \033[32m
COLOR_TEXT    = #\033[90m
COLOR_COMMENT = \033[90m
COLOR_RESET   = \033[0m
PADDING_SIZE  = 12
help:
	#$(MAKEFILE_LIST)
	grep -E '^[a-zA-Z_-]+:.*?## .*$$' lancement_ansible/Makefile | awk 'BEGIN {FS = ":.*?## "}; {printf ($$1 ~ "--" ? "${COLOR_SECTION}" $$2 "${COLOR_RESET}\n" : "${COLOR_TARGET}%-${PADDING_SIZE}s ${COLOR_RESET}${COLOR_TEXT}%s${COLOR_RESET}\n", $$1,  $$2)}'
	echo
	echo -e "${COLOR_SECTION}------------------------------------------------------------------------------------------${COLOR_RESET}"
	echo
	echo -e "PLX=${PLX} ${COLOR_COMMENT}# defined by $(origin PLX)${COLOR_RESET}"
	echo
.DEFAULT_GOAL := help
