#!/bin/bash
# set -x
cd $(dirname $(realpath $0))

if [ -z ${PIP_PROXY+x} ];
  then 
    echo "NO PIP Proxy to configure"
  else
    echo '[global] \n\
timeout = 60 \n\
index-url = http://${PROXY_FQDN}${PROXY_ROOT_PATH}${PIP_PROXY_PATH}simple \n\
index = http://${PROXY_FQDN}${PROXY_ROOT_PATH}${PIP_PROXY_PATH}pypi \n\
trusted-host = ${PROXY_FQDN}' > /etc/pip.conf
fi

pip list | grep niet

if [ $? -ne 0 ]; then
  pip3 install niet
fi

# This is where you want to clone your repo project.
DEFAULT_ROLES_PATH=${DEFAULT_ROLES_PATH:-$HOME/.ansible/roles}

cd ..
PROJECT_DIR=$PWD
PLAYBOOK_DIR=$PROJECT_DIR/playbook

for req in $(python -m niet . $PLAYBOOK_DIR/requirements.yml -f json | sed 's/git+http/http/g' | jq -c '.[]')
do
  SRC=$(echo $req | jq -r '.src')
  VERSION=$(echo $req | jq -r '.version')
  NAME=$(echo $req | jq -r '.name')
  if [ -n "$VERSION" ]; then
    BRANCH="--branch $VERSION"
  fi
  cd $DEFAULT_ROLES_PATH
  if [ -d "$DEFAULT_ROLES_PATH/$NAME" ]; then
    if [ -d "$DEFAULT_ROLES_PATH/$NAME/.git" ]; then
      echo "#### Projet: $DEFAULT_ROLES_PATH/$NAME ####"
      echo "#### git checkout $VERSION ####"
      cd $NAME
      git pull --all
      git checkout $VERSION
    else
      echo "Le répertoire $DEFAULT_ROLES_PATH/$NAME n'est pas un dépot git. Traiter le sujet manuellement..."
    fi
  else
    echo "#### git clone $SRC $BRANCH $NAME ####"
    git clone $SRC $BRANCH $NAME
  fi
done
